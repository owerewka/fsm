package sample.fsm

import akka.actor._

import scala.concurrent.duration._

sealed trait FSMHakkerMessage

object Eat extends FSMHakkerMessage

sealed trait FSMHakkerState

case object Waiting extends FSMHakkerState

case object Eating extends FSMHakkerState

class FSMHakker(name: String) extends Actor with FSM[FSMHakkerState, String] {

  startWith(Waiting, "glodny")

  when(Waiting) {
    case Event(Eat, _) =>
      println("%s I'm waiting now".format(name))
      goto(Eating) using "yyy" forMax 5.second
  }

  when(Eating) {
    case Event(StateTimeout, _) =>
      println("%s stop eating you are full".format(name))
      sender ! Eat
      goto(Waiting) using "DUPA" forMax 5.seconds
  }

  initialize()

}

/*
* Alright, here's our test-harness
*/
object DiningHakkersOnFsm extends App {

  val system = ActorSystem()

  val oskar = system.actorOf(Props(classOf[FSMHakker], "Oskar"))
  val kazek = system.actorOf(Props(classOf[FSMHakker], "Kazek"))

  oskar ! Eat
  kazek ! Eat

}
